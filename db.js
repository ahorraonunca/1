  const items = [
    {
      id: 0,
      name: 'Sabamemem',
      age: 25,
      job: 'Phillippines',
      images: ['images/testpic4.jpg', 'images/testpic2.jpg', 'images/testpic1.jpg'],
      distance: 25,
      description: 'Swipe for me! You’ll find me very ap-peel-ing'
    },
    {
      id: 1,
      name: 'Plantain',
      age: 28,
      job: 'Nicaragua',
      images: ['images/testpic2.jpg', 'images/testpic4.jpg'],
      distance: 4,
      description: 'Lo at.'
    },
    {
      id: 2,
      name: 'Banan',
      age: 21,
      job: 'Finnland',
      images: ['images/testpic3.jpg', 'images/testpic2.jpg'],
      distance: 9,
      description: 'I like fruits!'
    },
    {
      id: 3,
      name: 'Actually an orange',
      age: 12,
      job: 'Scammer',
      images: ['images/testpic1.jpg', 'images/testpic2.jpg'],
      distance: 2455,
      description: 'Follow me on my totally banana-related Instagram 📸'
    }
  ];